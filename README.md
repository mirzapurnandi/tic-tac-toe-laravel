# TIC TAC TOE LARAVEL

Aplikasi sederhana untuk permainan tic-tac-toe di Laravel.

## Installation

Download terlebih dahulu git ini dengan cara

```bash
git clone https://gitlab.com/mirzapurnandi/tic-tac-toe-laravel.git
```

## Usage

```bash
- composer install
- cp .env-example .env
- php artisan key:generate
- php artisan serve
```

## Link Permainan

```bash
http://127.0.0.1:8000/home
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
