<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tic Tac Toe Laravel</title>

    <link rel="stylesheet" href="{{ asset('assets/styles.css') }}">
    <script src="{{ asset('assets/scripts.js') }}" defer></script>
</head>
<body>
    <div class="board" id="board">
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
        <div class="cell" data-cell></div>
    </div>

    <div class="winning-message" id="winningMessage">
        <div data-winning-message-text></div>
        <button id="restartButton">Start Again!</button>
    </div>
</body>
</html>
